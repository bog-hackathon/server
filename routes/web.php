<?php

Route::group(['middleware' => 'guest'], function () {
    Route::get('/', 'Web\GuestController@home');
    Route::get('login', 'Web\GuestController@getLogin');
    Route::post('login', 'Web\GuestController@postLogin');
});

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'ajax'], function () {
        Route::get('/numberOfPayments', 'Web\AjaxController@numberOfPayments');
        Route::get('/amountOfPayments', 'Web\AjaxController@amountOfPayments');
    });
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', 'Web\DashboardController@index');

        Route::group(['prefix' => 'promotions'], function () {
            Route::get('/', 'Web\PromotionsController@index');
            Route::post('/', 'Web\PromotionsController@store');
        });
    });
});

Route::group(['prefix' => 'api'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'Api\AuthController@login');
        Route::post('register', 'Api\AuthController@register');
    });

    Route::group(['middleware' => ['auth.api']], function () {

        Route::group(['prefix' => 'payments'], function () {
            Route::get('/', 'Api\PaymentController@index');
            Route::post('/', 'Api\PaymentController@store');
            Route::get('{id}', 'Api\PaymentController@get');
        });

        Route::group(['prefix' => 'transactions'], function () {
            Route::get('/', 'Api\TransactionController@index');
            Route::post('/', 'Api\TransactionController@store');
            Route::get('{id}', 'Api\TransactionController@get');
        });

        Route::group(['prefix' => 'account'], function () {
            Route::put('/settings', 'Api\AccountController@update');
            Route::post('/profile', 'Api\AccountController@saveProfilePicture');
            Route::post('/services/bog/connect', 'Api\AccountController@connectBog');
            Route::get('/services/bog/balance', 'Api\AccountController@balanceBog');
        });
    });

});

Route::get('qr/{id}', 'Api\PaymentController@qr');
Route::get('profile/{id}', 'Api\AccountController@getProfilePicture');
