<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run () {
        \App\Payment::truncate();
        \App\Transaction::truncate();

        $users = \App\User::all();
        $faker = Faker\Factory::create();

        $payments     = [];
        $transactions = [];

        foreach ($users as $user) {
            $user->email = "user.$user->id@gmail.com";
            $user->save();
            $nPayments = $faker->numberBetween(300, 500);
            for ($paymentId = 1; $paymentId <= $nPayments; $paymentId++) {
                $nTransactions  = $faker->numberBetween(1, 2);
                $amount         = $faker->numberBetween(1, 20) * 10;
                $perTransaction = $amount / $nTransactions;
                $dateTime       = $faker->dateTimeBetween('-2 weeks')->format('Y-m-d H:i:s');
                $payment        = [
                    'receiver_user_id' => $user->id,
                    'name'             => $faker->word . " #" . $faker->numberBetween(1000, 10000),
                    'amount'           => $amount,
                    'created_at'       => $dateTime,
                    'updated_at'       => $dateTime,
                ];
                $payments[]     = $payment;

                for ($j = 0; $j < $nTransactions; $j++) {
                    $transaction    = [
                        'amount'         => $perTransaction,
                        'sender_user_id' => $faker->numberBetween(1, 10),
                        'payment_id'     => $paymentId,
                        'created_at'     => $dateTime,
                        'updated_at'     => $dateTime,
                    ];
                    $transactions[] = $transaction;
                }
            }
        }

        \App\Payment::insert($payments);
        \App\Transaction::insert($transactions);
    }
}
