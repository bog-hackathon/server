<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Mapay</title>

    <link rel="apple-touch-icon" href="/assets/images/logo/apple-touch-icon.png">
    <link rel="shortcut icon" href="/assets/images/logo/favicon.png">

    <link rel="stylesheet" href="/assets/vendor/bootstrap/dist/css/bootstrap.css"/>
    <link rel="stylesheet" href="/assets/vendor/PACE/themes/blue/pace-theme-minimal.css"/>
    <link rel="stylesheet" href="/assets/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/themify-icons.css" rel="stylesheet">
    <link href="/assets/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="/assets/css/animate.min.css" rel="stylesheet">
    <link href="/assets/css/app.css" rel="stylesheet">

</head>
<body>
<div class="app">
    @section('app')
        <div class="layout">
            <div class="page-container">
                <div class="main-content">
                    @yield('page')
                </div>
            </div>
        </div>
    @show
</div>

<script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
<script src="/assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="/assets/vendor/PACE/pace.min.js"></script>
<script src="/assets/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/assets/vendor/d3/d3.min.js"></script>
<script src="/assets/js/app.js"></script>

</body>
</html>
