<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Mapay</title>

    <link rel="apple-touch-icon" href="/assets/images/logo/apple-touch-icon.png">
    <link rel="shortcut icon" href="/assets/images/logo/favicon.png">

    <link rel="stylesheet" href="/assets/vendor/bootstrap/dist/css/bootstrap.css"/>
    <link rel="stylesheet" href="/assets/vendor/PACE/themes/blue/pace-theme-minimal.css"/>
    <link rel="stylesheet" href="/assets/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/themify-icons.css" rel="stylesheet">
    <link href="/assets/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="/assets/css/animate.min.css" rel="stylesheet">
    <link href="/assets/css/app.css" rel="stylesheet">

</head>
<body>
<div class="app">
    <div class="layout">
        <div class="header navbar">
            <div class="header-container">
                <div class="nav-logo">
                    <a href="/dashboard">
                        <h1 class="font-weight-light font-size-35">Mapay</h1>
                    </a>
                </div>
            </div>
        </div>
        <!-- Header END -->

        <!-- Side Nav START -->
        <div class="side-nav expand-lg">
            <div class="side-nav-inner">
                <ul class="side-nav-menu scrollable">
                    <li class="side-nav-header">
                        <span>Navigation</span>
                    </li>
                    <li class="nav-item">
                        <a href="/dashboard"><span class="icon-holder"><i class="mdi mdi-gauge"></i></span><span class="title">Dashboard</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="/dashboard/promotions"><span class="icon-holder"><i class="mdi mdi-magnet"></i></span><span class="title">Promotions</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="page-container">
            <div class="main-content">
                <div class="container-fluid">
                    @yield('page')
                </div>
            </div>
            <footer class="content-footer">
                <div class="footer">
                    <div class="copyright">
                        <span>Copyright © 2018 <b class="text-dark">Theme_Nate</b>. All rights reserved.</span>
                        <span class="go-right">
                                <a href="" class="text-gray m-r-15">Term &amp; Conditions</a>
                                <a href="" class="text-gray">Privacy &amp; Policy</a>
                            </span>
                    </div>
                </div>
            </footer>

        </div>
    </div>
</div>

<script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
<script src="/assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="/assets/vendor/PACE/pace.min.js"></script>
<script src="/assets/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/assets/vendor/d3/d3.min.js"></script>
<script src="/assets/js/app.js"></script>
@yield('javascript')
</body>
</html>
