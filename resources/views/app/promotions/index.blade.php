@extends('master-dashboard')
@section('page')
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header border bottom">
                    <h4 class="card-title">Start new promotion</h4>
                </div>
                <div class="card-body">
                    <form class="m-t-15" action="/dashboard/promotions" method="post">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" placeholder="Promotion name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label control-label">Message</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="message" placeholder="You are eligible for a free Pizza!">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label control-label">Amount</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="min_amount" placeholder="Minimal amount to apply promotion">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-2 offset-10">
                                <div class="text-sm-right">
                                    <button class="btn btn-gradient-success">Start</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header border bottom">
                    <h4 class="card-title">Promotions withdrawn</h4>
                </div>
                <div class="card-body">
                    <canvas class="chart" id="stacked-area-chart" style="height: 270px"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header border bottom">
                    <h4 class="card-title">
                        Live promotions
                    </h4>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Message</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Date</th>
                        <th scope="col"></th>
                        </thead>
                        <tbody>
                        @foreach($promotions as $promotion)
                            <tr>
                                <td scope="row">{{$promotion->id}}</td>
                                <td>{{$promotion->name}}</td>
                                <td>{{$promotion->message}}</td>
                                <td>{{$promotion->min_amount}}</td>
                                <td>{{$promotion->created_at->diffForHumans()}}</td>
                                <td>
                                    <button class="btn btn-icon btn-danger">
                                        <i class="mdi mdi-delete-circle"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="/assets/vendor/chart.js/dist/Chart.min.js"></script>
    <script>

		(function ($, window) {

			var chartJs = {};

			chartJs.init = function () {
				var stackedAreaChart = document.getElementById("stacked-area-chart");
				var stackedAreaCtx = stackedAreaChart.getContext('2d');
				stackedAreaChart.height = 177;
				var stackedAreaConfig = new Chart(stackedAreaCtx, {
					type: 'line',
					data: {
						labels:   [
							"1 Sep",
							"2 Sep",
							"3 Sep",
							"4 Sep",
							"5 Sep",
							"6 Sep",
							"7 Sep",
							"8 Sep",
							"9 Sep",
							"10 Sep",
							"11 Sep",
							"12 Sep",
							"13 Sep",
							"14 Sep",
							"15 Sep",
                        ],
						datasets: [{
							label:                'Series A',
							backgroundColor:      app.colors.successOpacity,
							borderColor:          app.colors.success,
							pointBackgroundColor: app.colors.success,
							borderWidth:          2,
							data:                 [
								65,
                                59,
                                80,
                                81,
                                56,
                                55,
                                40,
								65,
                                59,
                                80,
                                81,
                                56,
                                55,
                                40,
								65,
                            ]
						}]
					},

					options: {
						legend: {
							display: false
						}
					}
				});
			}
			window.chartJs = chartJs;

		})(jQuery, window);

		// initialize app
		+function ($) {
			chartJs.init();
		}(jQuery);
    </script>
@endsection
