@extends('master-simple')

@section('app')
    <div class="layout bg-gradient-info">
        <div class="container">
            <div class="row full-height align-items-center">
                <div class="col-md-7 d-none d-md-block">
                    <div class="m-t-15 m-l-20">
                        <h1 class="font-weight-light font-size-35 text-white">Mapay</h1>
                        <p class="text-white width-70 text-opacity m-t-25 font-size-16">Pay with confidence</p>
                        <div class="m-t-60">
                            <a href="" class="text-white text-link m-r-15">Term &amp; Conditions</a>
                            <a href="" class="text-white text-link">Privacy &amp; Policy</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card card-shadow">
                        <div class="card-body">
                            <div class="p-h-15 p-v-40">
                                <h2>Login</h2>
                                <p class="m-b-15 font-size-13">Please enter your email and password to login</p>
                                <form method="POST" action="/login">
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control" placeholder="Password">
                                    </div>
                                    <button class="btn btn-block btn-lg btn-gradient-success">Login</button>
                                    <div class="text-center m-t-30">
                                        <a href="" class="text-gray text-link text-opacity">Forget Password?</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
