@extends('master-dashboard')
@section('page')
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header border bottom">
                    <h4 class="card-title">Payments</h4>
                </div>
                <div class="card-body">
                    <p>Number of payments made on your account for each day.</p>
                    <div class="m-t-25">
                        <canvas class="chart" id="number-of-payments"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header border bottom">
                    <h4 class="card-title">Payments</h4>
                </div>
                <div class="card-body">
                    <p>Amount of payments made on your account for each day.</p>
                    <div class="m-t-25">
						<canvas class="chart" id="amount-of-payments"></canvas>
					</div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="/assets/vendor/chart.js/dist/Chart.min.js"></script>
    <script>
		function renderLineChart (domId, labels, values) {
			let lineChart = document.getElementById(domId);
			let lineCtx = lineChart.getContext('2d');
			lineChart.height = 120;
			new Chart(lineCtx, {
				type:    'line',
				data:    {
					labels:   labels,
					datasets: [{
						backgroundColor:      app.colors.transparent,
						borderColor:          app.colors.success,
						pointBackgroundColor: app.colors.success,
						borderWidth:          2,
						data:                 values
					}]
				},
				options: {
					legend: {
						display: false
					},
					scales: {
						xAxes: [{
							ticks: {
								autoSkip:    false,
								maxRotation: 90,
								minRotation: 60
							}
						}],
						yAxes: [{
							ticks: {
								beginAtZero: true,
								callback:    function (value, index, values) {
									if (parseInt(value) >= 1000) {
										value = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									} else {
										value = value;
									}
									if (domId == 'amount-of-payments') {
										value = '$' + value;
									}
									return value;
								}
							}
						}]
					}
				}
			});
		}

		function loadChart (selector, endpoint, chartBuilder) {
			$.ajax({
				url:     endpoint,
				success: (response) => {
					chartBuilder(selector, response.labels, response.values);
				}
			});
		}

		$(function () {
			loadChart('number-of-payments', '/ajax/numberOfPayments', renderLineChart);
			loadChart('amount-of-payments', '/ajax/amountOfPayments', renderLineChart);
		});
    </script>
@endsection
