FROM hitalos/laravel:latest

WORKDIR /var/www

COPY . /var/www
RUN composer install

RUN php artisan key:generate

RUN composer dump-autoload
RUN php artisan optimize

CMD php artisan serve --host=0.0.0.0 --port 8080
