<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model {
    protected $table = 'promotion';
    protected $fillable = [
        'name',
        'message',
        'min_amount',
    ];

    public function user () {
        return $this->belongsTo(User::class);
    }
}
