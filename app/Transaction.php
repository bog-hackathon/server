<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

    protected $fillable = [
        'amount',
        'sender_user_id',
        'payment_id',
    ];

    public function sender () {
        return $this->hasOne(User::class, 'id', 'sender_user_id');
    }

    public function payment () {
        return $this->belongsTo(Payment::class);
    }
}
