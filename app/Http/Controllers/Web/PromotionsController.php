<?php

namespace App\Http\Controllers\Web;

use App\Promotion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromotionsController extends Controller {
    public function index (Request $request) {
        return view('app.promotions.index', [
            'promotions' => $request->user()->promotions,
        ]);
    }

    public function store (Request $request) {
        $promotion          = new Promotion($request->all());
        $promotion->user_id = $request->user()->id;
        $promotion->save();
        return redirect()->back();
    }
}
