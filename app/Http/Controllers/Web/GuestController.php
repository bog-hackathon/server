<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GuestController extends Controller {
    public function home () {
        return redirect('/login');
    }

    public function getLogin () {
        return view('app/login');
    }

    public function postLogin (Request $request) {
        if (Auth::attempt($request->all())) {
            return redirect()->intended('/dashboard');
        }

        return redirect()->back()->withInput($request->except('password'));
    }
}
