<?php

namespace App\Http\Controllers\Web;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AjaxController extends Controller {
    public function numberOfPayments (Request $request) {
        $data = $request->user()->paymentsReceived()->orderBy('created_at', 'asc')->get()->groupBy(function ($payment) {
            return Carbon::parse($payment->created_at)->format('j M');
        })->map(function ($payments, $label) {
            return collect($payments)->count();
        })->toArray();

        return [
            'labels' => array_keys($data),
            'values' => array_values($data),
        ];
    }

    public function amountOfPayments (Request $request) {
        $data = $request->user()->paymentsReceived()->orderBy('created_at', 'asc')->get()->groupBy(function ($payment) {
            return Carbon::parse($payment->created_at)->format('j M');
        })->map(function ($payments, $label) {
            return collect($payments)->sum(function ($payment) {
                return $payment->amount;
            });
        })->toArray();

        return [
            'labels' => array_keys($data),
            'values' => array_values($data),
        ];
    }

}
