<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller {

    public function login (Request $request) {
        if (Auth::attempt($request->all())) {
            $user = User::where('email', $request->email)->first();
            $user->resetToken();
            return $user->makeVisible('token');
        }

        return response(['error' => 'invalid email/password'], 403);
    }

    public function register (Request $request) {
        $data             = $request->all();
        $data['password'] = bcrypt($data['password']);
        $user             = User::create($data);
        $user->resetToken();
        return $user->makeVisible('token');
    }

}
