<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Payment;
use Illuminate\Http\Request;
use LaravelQRCode\Facades\QRCode;

class PaymentController extends Controller {

    public function index (Request $request) {
        return $request->user()->paymentsReceived()->with('transactions.sender')->orderBy('created_at', 'desc')->limit(20)->get();
    }

    public function get ($id, Request $request) {
        return $request->user()->paymentsReceived()->with('transactions.sender', 'receiver')->orderBy('created_at', 'desc')->limit(20)->find($id);
    }

    public function store (Request $request) {
        $payment = Payment::create([
            "name"             => $request->get('name'),
            "amount"           => $request->get('amount'),
            'receiver_user_id' => $request->user()->id,
        ]);

        return $payment;
    }

    public function qr ($id, Request $request) {
        return QRCode::text($id)->png();
    }

}
