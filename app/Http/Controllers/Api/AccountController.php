<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\BogService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AccountController extends Controller {
    public function update (Request $request) {
        $user = $request->user();
        $user->fill($request->all());
    }

    public function connectBog (Request $request) {
        $service = new BogService($request->get('username'), $request->get('password'));
        $balance = $service->getBalance();

        $user               = $request->user();
        $user->bog_username = $request->get('username');
        $user->bog_password = $request->get('password');
        $user->save();

        return [
            "balance" => $balance,
            "bonus" => 0
        ];
    }

    public function balanceBog (Request $request) {
        $service = new BogService($request->user()->bog_username, $request->user()->bog_username);
        return [
            "balance" => $service->getBalance(),
            "bonus" => 0
        ];
    }

    public function saveProfilePicture (Request $request) {
        $user     = $request->user();
        $fileName = $user->id;
        $request->image->move(storage_path('profile_pictures'), $fileName);
        $user->save();
        return $user;
    }

    public function getProfilePicture ($id, Request $request) {
        if (file_exists(storage_path("profile_pictures/$id"))) {
            return response()->file((storage_path("profile_pictures/$id")));
        }

        $user     = User::find($id);
        $fullName = $user->first_name . ' ' . $user->last_name;

        $path = "/tmp/" . md5(uniqid());
        file_put_contents($path, file_get_contents("https://ui-avatars.com/api/?name=" . $fullName . "&size=400&color=3687dc&background=f8f9fa"));

        return response()->file($path);

    }

}
