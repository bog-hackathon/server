<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Payment;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller {

    public function index (Request $request) {
        return $request->user()->transactionsSent()->with('sender', 'payment')->orderBy('created_at', 'desc')->limit(20)->get();
    }

    public function store (Request $request) {

        $payment_id = $request->get('payment_id');
        $payment    = Payment::find($payment_id);

        $paidAmount = 0;
        foreach ($payment->transactions as $key => $transaction) {
            $paidAmount += $transaction->amount;
        }

        if ($payment->amount == $paidAmount) {
            return response([
                "error" => "This payment is paid",
            ]);
        }

        if ($paidAmount + $request->get('amount') > $payment->amount) {
            return response([
                "error" => "The amount you entered exceeds pending amount on payment",
                "_meta" => [
                    "pendingAmount" => $payment->amount - $paidAmount,
                ],
            ]);
        }

        $transaction = Transaction::create([
            "sender_user_id" => $request->user()->id,
            "payment_id"     => $payment_id,
            "amount"         => $request->get('amount'),
        ]);

        return $transaction;
    }

    public function get ($id, Request $request) {
        return $request->user()->transactionsSent()->with('sender', 'payment')->orderBy('created_at', 'desc')->limit(20)->find($id);
    }
}
