<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class ApiAuthentication {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle ($request, Closure $next) {
        if (!$request->hasHeader('authorization')) {
            return response([
                'error' => 'you are not authenticated',
            ], 401);
        }

        $user = User::where('token', $request->header('authorization'))->first();
        if(empty($user)) {
            return response([
                'error' => 'you are not authenticated',
            ], 401);
        }
        $request->setUserResolver(function () use ($user) {
            return $user;
        });

        return $next($request);
    }
}
