<?php


namespace App\Services;


class BogService {

    private $host = "https://api.fintech.ge/";
    private $SessionId;
    private $client;
    private $username;
    private $password;

    public function __construct ($username, $password) {
        $this->client   = new \GuzzleHttp\Client(["base_uri" => $this->host]);
        $this->username = $username;
        $this->password = $password;
    }

    public function connect () {
        $response        = json_decode($this->client->request("GET", "api/Clients/Login/$this->username/$this->password")->getBody()->getContents());
        $this->SessionId = explode(".", $response->SessionId)[0];
        return $this;
    }

    public function getBalance () {
        if (empty($this->SessionId)) {
            $this->connect();
        }

        $balanceTotal = 0;
        $response     = json_decode($this->client->request("GET", "api/Products/AssetsAndLiabilities/$this->SessionId")->getBody()->getContents());

        foreach ($response->AvailableAmounts as $key => $obj) {
            $balanceTotal += $obj->AmountBase;
        }

        return $balanceTotal;
    }


}
