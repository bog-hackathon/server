<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'bog_username',
        'bog_password',
        'token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'token',
        'remember_token',
    ];

    public function transactionsSent () {
        return $this->hasMany(Transaction::class, 'sender_user_id', 'id');
    }

    public function paymentsReceived () {
        return $this->hasMany(Payment::class, 'receiver_user_id', 'id');
    }

    public function promotions () {
        return $this->hasMany(Promotion::class);
    }

    public function resetToken () {
        $this->token = str_random(64);
        $this->save();
    }
}
