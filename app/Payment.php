<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {
    protected $fillable = [
        'receiver_user_id',
        'name',
        'amount',
    ];

    protected $appends = [
        'transactions_sum',
    ];

    public function receiver () {
        return $this->hasOne(User::class, 'id', 'receiver_user_id');
    }

    public function transactions () {
        return $this->hasMany(Transaction::class);
    }

    public function getTransactionsSumAttribute () {
        $sum = 0;
        foreach ($this->transactions as $transaction) {
            $sum += $transaction->amount;
        }
        return $sum;
    }


}
